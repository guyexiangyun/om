package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
@Data
@TableName("member_sign_rule")
public class MemberSignRule implements Serializable {

	private static final long serialVersionUID = 1L;

    	/**
	 * id自增
	 */
		@TableId
		private Long id;
		/**
	 * 第一天签到得多少积分
	 */
		private Integer numOne;
		/**
	 * 第二天签到得多少积分
	 */
		private Integer numTwo;
		/**
	 * 第三天签到得多少积分
	 */
		private Integer numThree;
		/**
	 * 第四天签到得多少积分
	 */
		private Integer numFour;
		/**
	 * 第五天签到得多少积分
	 */
		private Integer numFive;
		/**
	 * 第六天签到得多少积分
	 */
		private Integer numSix;
		/**
	 * 第七天签到得多少积分
	 */
		private Integer numSeven;
	
}
